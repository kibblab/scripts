# URL Status Checker

Check links from a static html page or a text list. Creates a tab-separated value (TSV) report with their status.

Requires [curl cli](https://curl.se/) and [Powershell 7](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell?view=powershell-7.2)+.

> 🗒 **Note:** On Windows, change `-o /dev/null` to `-o NUL`

Use `.\linkchecker.ps1 -help` or `.\linkchecker.ps1 -h` for the commands.

**Basic usage:** Outputs a file called `linkstatus.tsv`.

```
pwsh linkchecker.ps1 README.md
```

**Explicit arguments:** specify the input path and output filename.

```
pwsh linkchecker.ps1 -path ./examples/test.json -o 2022-07-29.tsv
```

**Dry run:** show links to be checked without making network calls.

```
pwsh linkchecker.ps1 . -dryrun
```

This lists all the links found in the current directory. To actually run the checker: `pwsh linkchecker.ps1 .`

**Interpreting TSV results**

A TSV/CSV can be opened in Excel, Google and LibreOffice.

- A status between `400`-`499` is broken.
- `500`+ might be temporarily unavailable, not necessarily broken
- `000` means the link timed out after 10 seconds.

**Example:**

A PowerShell script to create a CSV named after today's date, only containing failed links.

```powershell
$input='./examples/links.txt'

# Check links
$TempFile = New-TemporaryFile;
.\linkchecker.ps1 -path $input -o $TempFile;

# Filter for bad links
$errored=( Import-Csv -Path $TempFile -Delimiter "`t" | Sort-Object status | Where-Object {$_.status -gt 399 -or $_.status -lt 100 } );

# Name a CSV report after today's date.
$date=( Get-Date -Format 'yyyy-MM-dd' );
$errored | Export-Csv "$date.csv"

Remove-Item -Recurse -Force $TempFile
```

## Reasons

There's many advanced broken link checkers, but they web scrape with UI automation for the worst case scenario, like websites that have fancy dynamic client-sided rendering where you have to wait for the JavaScript to inject the HTML elements,
and the only way to find the URLS on the page is to run a headless browser,
and some of these link checkers will also recursively crawl through and check the URLs of every connected page.

That's a lot of overhead for a documentation site, which is simpler and page counts are known already.

### One-liners

If you don't need a full-fledged script, in Linux, you can use a 1-liner to get the results in the console:

```bash
xargs -P 100 -n 1 curl --connect-timeout 10 -Iso /dev/null -w "%{url_effective}\t%{http_code}\t%{redirect_url}\n" < ./examples/links.txt
```

The PowerShell 1-liner equivalent would be...

```powershell
Get-Content ./examples/links.txt | ForEach-Object -Parallel{ curl --connect-timeout 10 -Iso /dev/null -w "%{url_effective}\t%{http_code}\t%{redirect_url}\n" $_ } -ThrottleLimit 100
```

... but it's verbose.

> 🗒 **Note:** On Windows, change `-o /dev/null` to `-o NUL`

## References

- [Bulk URL Checker with cURL ](https://www.christopheryee.org/blog/bulk-url-checker-curl/)
- JSON test file comes from [ajv json validator](https://github.com/ajv-validator/ajv/tree/master/spec/remotes)