param(
  [Alias('p')]
  [string]$path,
  [Alias('o')]
  [string]$output = '.\linkstatus.tsv',
  [switch]$dryrun,
  [Alias('h')]
  [switch]$help
)

$accept=@( 'html', 'htm', 'markdown', 'md', 'yaml', 'yml', 'json', 'xml', 'svg')

if ($help){
@"

  SYNOPSIS
    Check broken links from a static html page or a text list. Creates a TSV file with their status.
    File extensions checked: $accept

  USAGE
    linkchecker.ps1 -path PATH [-output OUTPUT]

    -p, -path         The input path. Can be a directory and will recurse through children
    -o, -output       The output TSV file. Default is '$output'
    -dryrun           Show links to be checked but don't make any HTTP calls
    -h, -help         Guidance menu
"@
Exit 0
}

$rgx='(http|https)://[^\"<>{}\(\)\t,]+'

# Check if folder
if ((Get-Item $path).PSIsContainer) {
  $files=(Get-ChildItem . -Recurse -File).FullName
  $links=($files | ForEach-Object -Parallel{
    Get-Content $_ |  Select-String $($using:rgx) -All | foreach {$_.Matches.Value}
  } -ThrottleLimit 100 )
} else {
  $ext=(Split-Path -Path $path -Leaf).Split(".")[1];
  if ($ext -in $accept ){
    $links=( Get-Content $path |  Select-String $rgx -All | foreach {$_.Matches.Value} )
  } else {
    $links=@( Get-Content $path )
  }
}

$links = ($links | Sort-Object | Get-Unique)

if ($dryrun) { Write-Output $links; Exit 0 }

Write-Output "Checking $($links.length) URLs..."

$callTime = Measure-Command {
  Set-Content -Path $output "url`tstatus`tredirect"
  $links | ForEach-Object -Parallel{
    curl --connect-timeout 10 -Iso /dev/null -w "%{url_effective}\t%{http_code}\t%{redirect_url}\n" $_
  } -ThrottleLimit 100  | Add-Content -Path $output 
}


Write-Output "Done. Time to complete was $callTime"