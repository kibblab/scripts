Param(
	[string]$version='2.1',
	[Alias("h")]
	[switch]$help
	)
if ($help){
@"
SYNOPSIS:  Gets the list of WCAG success criteria sorted by conformance level.
			Requires pandoc. Outputs a file `wcag-X.X-levels.md`
USAGE:
	fetch.ps1 [-version VERSION]

	-version      The WCAG specification version. Default: '2.1'      
"@
Exit 0
}


if ($version -lt 3.0) {
  $v=($version -replace '\.', '')
  $base="https://www.w3.org/TR/WCAG$v/"
} else {
  $base='https://www.w3.org/TR/wcag-3.0/'
}

$spreadsheet="wcag-$version-compliance.csv"

function extract-section{
	Param( [string]$path)
	$level=($c | Select-String "\(Level (?<level>A{1,3})\)" | %{ $_.Matches[0].Groups['level'].Value })
	if ($level) { 
		$c | Select-String 'Success Criterion (?<section>\d\.\d\.\d) (?<title>[^\[\]]*)' | ForEach-Object {
			$section=($_.Matches[0].Groups['section'].Value)
			$title=($_.Matches[0].Groups['title'].Value)
			$slug=($title.toLower() -replace '[\(\)]','' -replace '\W', '-')
			$link="https://www.w3.org/WAI/WCAG$v/Understanding/$slug"
			# 'https://www.w3.org/WAI/WCAG21/Understanding/section-title-slug'
			$t=("{0} [{1}]({2})  " -f $section, $title, $link)
			$t | Add-Content -Path "$level.md"
			#"$level,$section,`"=HYPERLINK(`"`"$link`"`", `"`"$title`"`")`",,," | Add-Content -Path "worksheet.csv"
		};
		Move-Item -Path $path -Destination $level
	}
}

# download the latest Wcag, save as html, convert to plain-text
New-Item -Type Directory tmp -ErrorAction SilentlyContinue;
$htmlfile="./tmp/$version.md"
$pandocTemp="./tmp/$version.md"
(Invoke-Webrequest -URI $base).Content | Out-File $htmlfile
pandoc -f html -t gfm-raw_html -o $pandocTemp $htmlfile &&

# sort sections by level
New-Item -Type Directory sections -ErrorAction SilentlyContinue
(Get-Content -Raw $pandocTemp) -split '####' | Set-Content -LiteralPath { '.\sections\outFile{0:d2}.md' -f $script:index++ }

cd sections
New-Item -Type Directory A -ErrorAction SilentlyContinue
New-Item -Type Directory AA -ErrorAction SilentlyContinue
New-Item -Type Directory AAA -ErrorAction SilentlyContinue

# Create TOC
"# WCAG $version Conformance Levels" | Set-Content 'toc.md'
Get-Content $PSScriptRoot\intro.md | Add-Content 'toc.md'
'## Level A' | Set-Content 'A.md'
'## Level AA' | Set-Content 'AA.md'
'## Level AAA' | Set-Content 'AAA.md'
# 'Level,Section,Criteria,When,Who,Status' | Set-Content 'worksheet.csv'

(Get-Item *.md).FullName | %{ $c=(Get-Content $_);
	extract-section -path $_
}

Get-Content 'A.md' | Add-Content 'toc.md';
Get-Content 'AA.md' | Add-Content 'toc.md';
Get-Content 'AAA.md' | Add-Content 'toc.md';

Get-Content $PSScriptRoot\outro.md | Add-Content 'toc.md';
"`n> This page was generated on $( [DateTime]::UtcNow.ToString('f') )" | Add-Content 'toc.md';

cd ..

New-Item -Type Directory dist -ErrorAction SilentlyContinue
Move-Item -Path 'sections/toc.md' -Destination dist\"wcag-$version-levels.md" -Force
# Move-Item -Path 'sections/worksheet.csv' -Destination $spreadsheet -Force

Remove-Item tmp -Recurse -Force
Remove-Item sections -Recurse -Force