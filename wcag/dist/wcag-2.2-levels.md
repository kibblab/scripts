# WCAG 2.2 Conformance Levels
This page organizes the Web Content Accessibility Guidelines (WCAG) success criteria by conformance level into a list. This meant to help you work your way up to AA and AAA.

[What are levels of conformance?](#what-are-levels-of-conformance)

The WCAG is organized according to Perceivable, Operable, Understandable, Robust (POUR) principles, but in practice, developers and managers are focused on meeting requirements. Organizing the criteria based on ease and complexity is more suited for work.
## Level A
1.1.1 [Non-text Content](https://www.w3.org/WAI/WCAG22/Understanding/non-text-content)  
1.2.1 [Audio-only and Video-only (Prerecorded)](https://www.w3.org/WAI/WCAG22/Understanding/audio-only-and-video-only-prerecorded)  
1.2.2 [Captions (Prerecorded)](https://www.w3.org/WAI/WCAG22/Understanding/captions-prerecorded)  
1.2.3 [Audio Description or Media Alternative (Prerecorded)](https://www.w3.org/WAI/WCAG22/Understanding/audio-description-or-media-alternative-prerecorded)  
1.3.1 [Info and Relationships](https://www.w3.org/WAI/WCAG22/Understanding/info-and-relationships)  
1.3.2 [Meaningful Sequence](https://www.w3.org/WAI/WCAG22/Understanding/meaningful-sequence)  
1.3.3 [Sensory Characteristics](https://www.w3.org/WAI/WCAG22/Understanding/sensory-characteristics)  
1.4.1 [Use of Color](https://www.w3.org/WAI/WCAG22/Understanding/use-of-color)  
1.4.2 [Audio Control](https://www.w3.org/WAI/WCAG22/Understanding/audio-control)  
2.1.1 [Keyboard](https://www.w3.org/WAI/WCAG22/Understanding/keyboard)  
2.1.2 [No Keyboard Trap](https://www.w3.org/WAI/WCAG22/Understanding/no-keyboard-trap)  
2.1.4 [Character Key Shortcuts](https://www.w3.org/WAI/WCAG22/Understanding/character-key-shortcuts)  
2.2.1 [Timing Adjustable](https://www.w3.org/WAI/WCAG22/Understanding/timing-adjustable)  
2.2.2 [Pause, Stop, Hide](https://www.w3.org/WAI/WCAG22/Understanding/pause--stop--hide)  
2.3.1 [Three Flashes or Below Threshold](https://www.w3.org/WAI/WCAG22/Understanding/three-flashes-or-below-threshold)  
2.4.1 [Bypass Blocks](https://www.w3.org/WAI/WCAG22/Understanding/bypass-blocks)  
2.4.2 [Page Titled](https://www.w3.org/WAI/WCAG22/Understanding/page-titled)  
2.4.3 [Focus Order](https://www.w3.org/WAI/WCAG22/Understanding/focus-order)  
2.4.4 [Link Purpose (In Context)](https://www.w3.org/WAI/WCAG22/Understanding/link-purpose-in-context)  
2.4.7 [Focus Visible](https://www.w3.org/WAI/WCAG22/Understanding/focus-visible)  
2.5.1 [Pointer Gestures](https://www.w3.org/WAI/WCAG22/Understanding/pointer-gestures)  
2.5.2 [Pointer Cancellation](https://www.w3.org/WAI/WCAG22/Understanding/pointer-cancellation)  
2.5.3 [Label in Name](https://www.w3.org/WAI/WCAG22/Understanding/label-in-name)  
2.5.4 [Motion Actuation](https://www.w3.org/WAI/WCAG22/Understanding/motion-actuation)  
3.1.1 [Language of Page](https://www.w3.org/WAI/WCAG22/Understanding/language-of-page)  
3.2.1 [On Focus](https://www.w3.org/WAI/WCAG22/Understanding/on-focus)  
3.2.2 [On Input](https://www.w3.org/WAI/WCAG22/Understanding/on-input)  
3.2.6 [Consistent Help](https://www.w3.org/WAI/WCAG22/Understanding/consistent-help)  
3.3.1 [Error Identification](https://www.w3.org/WAI/WCAG22/Understanding/error-identification)  
3.3.2 [Labels or Instructions](https://www.w3.org/WAI/WCAG22/Understanding/labels-or-instructions)  
3.3.9 [Redundant Entry](https://www.w3.org/WAI/WCAG22/Understanding/redundant-entry)  
4.1.1 [Parsing](https://www.w3.org/WAI/WCAG22/Understanding/parsing)  
4.1.2 [Name, Role, Value](https://www.w3.org/WAI/WCAG22/Understanding/name--role--value)  
## Level AA
1.2.4 [Captions (Live)](https://www.w3.org/WAI/WCAG22/Understanding/captions-live)  
1.2.5 [Audio Description (Prerecorded)](https://www.w3.org/WAI/WCAG22/Understanding/audio-description-prerecorded)  
1.3.4 [Orientation](https://www.w3.org/WAI/WCAG22/Understanding/orientation)  
1.3.5 [Identify Input Purpose](https://www.w3.org/WAI/WCAG22/Understanding/identify-input-purpose)  
1.4.3 [Contrast (Minimum)](https://www.w3.org/WAI/WCAG22/Understanding/contrast-minimum)  
1.4.4 [Resize Text](https://www.w3.org/WAI/WCAG22/Understanding/resize-text)  
1.4.5 [Images of Text](https://www.w3.org/WAI/WCAG22/Understanding/images-of-text)  
2.4.5 [Multiple Ways](https://www.w3.org/WAI/WCAG22/Understanding/multiple-ways)  
2.4.6 [Headings and Labels](https://www.w3.org/WAI/WCAG22/Understanding/headings-and-labels)  
2.5.7 [Dragging Movements](https://www.w3.org/WAI/WCAG22/Understanding/dragging-movements)  
2.5.8 [Target Size (Minimum)](https://www.w3.org/WAI/WCAG22/Understanding/target-size-minimum)  
3.1.2 [Language of Parts](https://www.w3.org/WAI/WCAG22/Understanding/language-of-parts)  
3.2.3 [Consistent Navigation](https://www.w3.org/WAI/WCAG22/Understanding/consistent-navigation)  
3.2.4 [Consistent Identification](https://www.w3.org/WAI/WCAG22/Understanding/consistent-identification)  
3.3.3 [Error Suggestion](https://www.w3.org/WAI/WCAG22/Understanding/error-suggestion)  
3.3.4 [Error Prevention (Legal, Financial, Data)](https://www.w3.org/WAI/WCAG22/Understanding/error-prevention-legal--financial--data)  
3.3.7 [Accessible Authentication](https://www.w3.org/WAI/WCAG22/Understanding/accessible-authentication)  
4.1.3 [Status Messages](https://www.w3.org/WAI/WCAG22/Understanding/status-messages)  
## Level AAA
1.2.6 [Sign Language (Prerecorded)](https://www.w3.org/WAI/WCAG22/Understanding/sign-language-prerecorded)  
1.2.7 [Extended Audio Description (Prerecorded)](https://www.w3.org/WAI/WCAG22/Understanding/extended-audio-description-prerecorded)  
1.2.8 [Media Alternative (Prerecorded)](https://www.w3.org/WAI/WCAG22/Understanding/media-alternative-prerecorded)  
1.2.9 [Audio-only (Live)](https://www.w3.org/WAI/WCAG22/Understanding/audio-only-live)  
1.3.6 [Identify Purpose](https://www.w3.org/WAI/WCAG22/Understanding/identify-purpose)  
1.4.6 [Contrast (Enhanced)](https://www.w3.org/WAI/WCAG22/Understanding/contrast-enhanced)  
1.4.7 [Low or No Background Audio](https://www.w3.org/WAI/WCAG22/Understanding/low-or-no-background-audio)  
1.4.8 [Visual Presentation](https://www.w3.org/WAI/WCAG22/Understanding/visual-presentation)  
1.4.9 [Images of Text (No Exception)](https://www.w3.org/WAI/WCAG22/Understanding/images-of-text-no-exception)  
2.1.3 [Keyboard (No Exception)](https://www.w3.org/WAI/WCAG22/Understanding/keyboard-no-exception)  
2.2.3 [No Timing](https://www.w3.org/WAI/WCAG22/Understanding/no-timing)  
2.2.4 [Interruptions](https://www.w3.org/WAI/WCAG22/Understanding/interruptions)  
2.2.5 [Re-authenticating](https://www.w3.org/WAI/WCAG22/Understanding/re-authenticating)  
2.2.6 [Timeouts](https://www.w3.org/WAI/WCAG22/Understanding/timeouts)  
2.3.2 [Three Flashes](https://www.w3.org/WAI/WCAG22/Understanding/three-flashes)  
2.3.3 [Animation from Interactions](https://www.w3.org/WAI/WCAG22/Understanding/animation-from-interactions)  
2.4.8 [Location](https://www.w3.org/WAI/WCAG22/Understanding/location)  
2.4.9 [Link Purpose (Link Only)](https://www.w3.org/WAI/WCAG22/Understanding/link-purpose-link-only)  
2.5.5 [Target Size (Enhanced)](https://www.w3.org/WAI/WCAG22/Understanding/target-size-enhanced)  
2.5.6 [Concurrent Input Mechanisms](https://www.w3.org/WAI/WCAG22/Understanding/concurrent-input-mechanisms)  
3.1.3 [Unusual Words](https://www.w3.org/WAI/WCAG22/Understanding/unusual-words)  
3.1.4 [Abbreviations](https://www.w3.org/WAI/WCAG22/Understanding/abbreviations)  
3.1.5 [Reading Level](https://www.w3.org/WAI/WCAG22/Understanding/reading-level)  
3.1.6 [Pronunciation](https://www.w3.org/WAI/WCAG22/Understanding/pronunciation)  
3.2.5 [Change on Request](https://www.w3.org/WAI/WCAG22/Understanding/change-on-request)  
3.3.5 [Help](https://www.w3.org/WAI/WCAG22/Understanding/help)  
3.3.6 [Error Prevention (All)](https://www.w3.org/WAI/WCAG22/Understanding/error-prevention-all)  
3.3.8 [Accessible Authentication (No Exception)](https://www.w3.org/WAI/WCAG22/Understanding/accessible-authentication-no-exception)  
## What are levels of conformance?

> WCAG 2.0 guidelines are categorized into three levels of conformance in order to meet the needs of different groups and different situations: A (lowest), AA (mid range), and AAA (highest)... Level A sets a minimum level of accessibility and does not achieve broad accessibility for many situations. For this reason, **UC recommends AA conformance for all Web-based information.**  
> --[University of California](https://www.ucop.edu/electronic-accessibility/standards-and-best-practices/levels-of-conformance-a-aa-aaa.html)

Level AA conformance includes 100% of A criteria and 100% of AA criteria.

> For Level AA conformance, the Web page satisfies all the Level A and Level AA Success Criteria, or a Level AA conforming alternate version is provided.
>
> --[Understanding Conformance Requirements](https://www.w3.org/WAI/WCAG21/Understanding/conformance)

## References

- [WCAG Implementation Quick Ref](https://www.w3.org/WAI/WCAG21/quickref/) for techniques and examples.
- [ARIA Techniques](https://www.w3.org/TR/WCAG20-TECHS/aria) for developer/HTML specific techniques.

> This page was generated on Monday, December 12, 2022 8:58 PM
