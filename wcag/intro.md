This page organizes the Web Content Accessibility Guidelines (WCAG) success criteria by conformance level into a list. This meant to help you work your way up to AA and AAA.

[What are levels of conformance?](#what-are-levels-of-conformance)

The WCAG is organized according to Perceivable, Operable, Understandable, Robust (POUR) principles, but in practice, developers and managers are focused on meeting requirements. Organizing the criteria based on ease and complexity is more suited for work.