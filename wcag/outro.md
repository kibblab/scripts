## What are levels of conformance?

> WCAG 2.0 guidelines are categorized into three levels of conformance in order to meet the needs of different groups and different situations: A (lowest), AA (mid range), and AAA (highest)... Level A sets a minimum level of accessibility and does not achieve broad accessibility for many situations. For this reason, **UC recommends AA conformance for all Web-based information.**  
> --[University of California](https://www.ucop.edu/electronic-accessibility/standards-and-best-practices/levels-of-conformance-a-aa-aaa.html)

Level AA conformance includes 100% of A criteria and 100% of AA criteria.

> For Level AA conformance, the Web page satisfies all the Level A and Level AA Success Criteria, or a Level AA conforming alternate version is provided.
>
> --[Understanding Conformance Requirements](https://www.w3.org/WAI/WCAG21/Understanding/conformance)

## References

- [WCAG Implementation Quick Ref](https://www.w3.org/WAI/WCAG21/quickref/) for techniques and examples.
- [ARIA Techniques](https://www.w3.org/TR/WCAG20-TECHS/aria) for developer/HTML specific techniques.